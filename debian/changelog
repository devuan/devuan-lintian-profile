devuan-lintian-profile (1.12) unstable; urgency=medium

  * Add future suite name Freia.
  * Ignore tag missing-systemd-timer-for-cron-script (Closes: #791).

 -- Mark Hindley <mark@hindley.org.uk>  Sat, 09 Sep 2023 18:09:02 +0100

devuan-lintian-profile (1.11) unstable; urgency=medium

  * Include spelling corrections from the parent profile.
  * Add devuans -> Devuan's spelling correction.
  * d/control: bump Standards Version (no changes).

 -- Mark Hindley <mark@hindley.org.uk>  Sat, 24 Dec 2022 14:20:43 +0000

devuan-lintian-profile (1.10) unstable; urgency=medium

  * Add codename experimental.
  * d/control: bump Standards Version (no changes).

 -- Mark Hindley <mark@hindley.org.uk>  Wed, 08 Jun 2022 13:31:07 +0100

devuan-lintian-profile (1.9) unstable; urgency=medium

  * Check Devuan specific d/control fields.
  * Check Devuan version numbering.
  * Add Devuan specific spelling checks.
  * Space separated control lists require lintian >= 2.105.
  * d/control: take over maintenance.

 -- Mark Hindley <mark@hindley.org.uk>  Wed, 09 Mar 2022 15:26:39 +0000

devuan-lintian-profile (1.8) unstable; urgency=medium

  * main.profile: use space separated list.

 -- Mark Hindley <mark@hindley.org.uk>  Wed, 20 Oct 2021 15:54:06 +0100

devuan-lintian-profile (1.7) unstable; urgency=medium

  * d/control:
    - expand description to specify this package is used on top
      of Debian profile.
    - remove unnecessary git Build-Depends
  * Fix typo in chimaera name.
  * Add codename excalibur.
  * d/gbp.conf: correctly set debian-branch.

 -- Mark Hindley <mark@hindley.org.uk>  Mon, 11 Oct 2021 11:05:56 +0100

devuan-lintian-profile (1.6) unstable; urgency=medium

  * Updated disabled tag name: changelog-should-mention-nmu ->
    no-nmu-in-changelog. Requires lintian >= 2.79.0.
  * d/control: update Vcs-* fields following Gitea migration.
  * d/gbp.conf: add debian-tag.

 -- Mark Hindley <mark@hindley.org.uk>  Mon, 22 Jun 2020 10:27:04 +0100

devuan-lintian-profile (1.5) unstable; urgency=medium

  * Fix source package format: native.
  * Add sensible bug number range for Devuan (10-10000).
  * Move Devuan-specific data to correct path
    /usr/share/lintian/vendors/devuan/.
  * Accept unstable as alternative suite to ceres.
  * Add future codename daedalus (Devuan 5).
  * d/control:
    - add myself to Uploaders.
    - update Vcs* fields.
    - add Origin: Devuan.
    - fix typo in description.
  * Fix lintian warnings:
    - d/control: add Depends: ${misc:Depends}.
    - d/copyright: remove boilerplate TODO text.

 -- Mark Hindley <mark@hindley.org.uk>  Tue, 28 Apr 2020 11:46:48 +0100

devuan-lintian-profile (1.4) unstable; urgency=medium

  * Fix disabled tags

 -- Daniel Reurich <centurion@haggai.centurion.net.nz>  Sun, 10 Feb 2019 19:33:48 +1300

devuan-lintian-profile (1.3) unstable; urgency=medium

  * stop nmu warnings

 -- Daniel Reurich <centurion@haggai.centurion.net.nz>  Thu, 22 Nov 2018 16:28:12 +1300

devuan-lintian-profile (1.2) unstable; urgency=medium

  * Remove systemd service file check

 -- Daniel Reurich <centurion@haggai.centurion.net.nz>  Fri, 09 Nov 2018 01:30:11 +1300

devuan-lintian-profile (1.1) unstable; urgency=medium

  * add Enhances lintian + reduce depends to suggests

 -- Daniel Reurich <centurion@haggai.centurion.net.nz>  Mon, 05 Nov 2018 21:40:32 +1300

devuan-lintian-profile (1.0) unstable; urgency=medium

  * Various minor updates:

 -- Daniel Reurich <centurion@haggai.centurion.net.nz>  Mon, 05 Nov 2018 21:06:17 +1300

devuan-lintian-profile (0.3) stable; urgency=medium

  * update tag
  * add git to build-depends

 -- Daniel Reurich <daniel@centurion.net.nz>  Thu, 21 May 2015 22:19:08 +0000

devuan-lintian-profile (0.2) stable; urgency=medium

  * add gbp.conf
  * fix upstream version
  * set arch to all
  * mv usr and remove src dir
  * add install
  * add provides lintian-vendor-profile

 -- Daniel Reurich <centurion@devuan.intranet.centurion.net.nz>  Thu, 21 May 2015 21:12:28 +0000

devuan-lintian-profile (0.1) unstable; urgency=low

  * Initial Release.

 -- Daniel Reurich <daniel@centurion.net.nz>  Thu, 21 May 2015 05:37:02 +0000
