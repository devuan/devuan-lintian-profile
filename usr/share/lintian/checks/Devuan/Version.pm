# devuan/version -- lintian check script (rewrite) -*- perl -*-
#
# Copyright © 2004 Marc Brockschmidt
# Copyright © 2021-2022 Felix Lechner
#
# Parts of the code were taken from the old check script, which
# was Copyright © 1998 Richard Braakman (also licensed under the
# GPL 2 or higher)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at http://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA.

package Lintian::Check::Devuan::Version;

use v5.20;
use warnings;
use utf8;

use Dpkg::Version;

use Moo;
use namespace::clean;

with 'Lintian::Check';

my %EXPLANATIONS_BY_PROHIBITED_PATTERN = (
  '\d [+] devuan' => '+devuanN no longer used, just use devuanN'
);

sub source {
    my ($self) = @_;

    my $fields = $self->processable->fields;

    return
      unless $fields->declares('Version');

    my $version = $fields->value('Version');

    my $dversion = Dpkg::Version->new($version);
    return
      unless $dversion->is_valid;

    my ($epoch, $upstream, $debian)
      = ($dversion->epoch, $dversion->version, $dversion->revision);

    for my $pattern (keys %EXPLANATIONS_BY_PROHIBITED_PATTERN) {

        my $explanation = $EXPLANATIONS_BY_PROHIBITED_PATTERN{$pattern};

        $self->hint('invalid-devuan-version', $version, "($explanation)")
          if $version =~ m{$pattern}x;
    }

    return;
}

1;

# Local Variables:
# indent-tabs-mode: nil
# cperl-indent-level: 4
# End:
# vim: syntax=perl sw=4 sts=4 sr et
