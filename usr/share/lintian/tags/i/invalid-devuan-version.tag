Tag: invalid-devuan-version
Severity: error
Check: devuan/version
Explanation: The version does not comply with the format required by Devuan.
 .
 Please check the hint annotation for more information.
